import React from "react";
import {Link} from "umi";

export default function() {
  return (
    <div>
      <Link to="/calculator" className="btn btn-primary">
        <button className="btn btn-primary">
          Calculator
        </button>
      </Link>
    </div>
  );
}
