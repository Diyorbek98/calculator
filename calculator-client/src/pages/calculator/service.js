import axios from 'axios'

export function getResults() {
  return axios.get('http://localhost:80/api/result')
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function saveResult(data) {
  return axios.post('http://localhost:80/api/result', data)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function deleteResult(data) {
  return axios.delete('http://localhost:80/api/result/'+data)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function deleteAllResult() {
  console.log(9999)
  return axios.delete('http://localhost:80/api/result')
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}
