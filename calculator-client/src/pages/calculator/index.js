import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'dva';
import 'bootstrap/dist/css/bootstrap.min.css';
import textarea from "eslint-plugin-jsx-a11y/src/util/implicitRoles/textarea";
import {Button, Icon, Modal, Table} from "antd";
import {deleteAllResult} from "./service";

@connect(({calculator}) => ({calculator}))
class Index extends Component {
  render() {

    const {calculator, dispatch} = this.props;
    const {displayValue, modalVisible, res, resultList, inputValue, signOp, equalKey} = calculator;


    const onKey = (val) => {
      if (signOp || equalKey) {
        dispatch({
          type: 'calculator/updateState',
          payload: {
            inputValue: val,
            signOp: false,
            equalKey: false
          }
        });
      } else {
        if (val.includes(".")) {
          if (!inputValue.includes(".")) {
            if (inputValue === '') {
              dispatch({
                type: 'calculator/updateState',
                payload: {
                  inputValue: "0."
                }
              });
            } else {
              dispatch({
                type: 'calculator/updateState',
                payload: {
                  inputValue: inputValue + val
                }
              });
            }
          }
        } else {
          dispatch({
            type: 'calculator/updateState',
            payload: {
              inputValue: inputValue + val
            }
          });
        }
      }
    };

    const onKeyOp = (val) => {
      console.log(displayValue)
      console.log(inputValue)
      if (!signOp) {
        dispatch({
          type: 'calculator/updateState',
          payload: {
            displayValue: displayValue + inputValue + val,
            inputValue: displayValue.includes("+") || displayValue.includes("*") || displayValue.includes("/") || displayValue.includes("-") ? eval(displayValue + inputValue) : '',
            signOp: true,
            equalKey: false
          }
        });
      }
    };

    const equalsKey = () => {
      if (inputValue !== '') {
        dispatch({
          type: 'calculator/saveResult',
          payload: {
            line: displayValue + inputValue + '=' + eval(displayValue + inputValue),
            res: eval(displayValue + inputValue)
          }
        });
        dispatch({
          type: 'calculator/updateState',
          payload: {
            inputValue: eval(displayValue + inputValue),
            displayValue: '',
            equalKey: true
          }
        });
      } else {
        alert("Iltimos raqamni kiriting!")
      }
    };

    const clearKey = () => {
      dispatch({
        type: 'calculator/updateState',
        payload: {
          inputValue: inputValue && inputValue.substring(0, inputValue.length - 1)
        }
      })
    };

    const allClearKey = () => {
      dispatch({
        type: 'calculator/updateState',
        payload: {
          inputValue: '',
          displayValue: ''
        }
      })
    };

    const handleMinus = () => {
      dispatch({
        type: 'calculator/updateState',
        payload: {
          inputValue: "(" + parseFloat(inputValue) * (-1) + ")"
        }
      })
    }


    const deleteResult = (index) => {
      dispatch({
        type: 'calculator/deleteResult',
        payload: index
      })
    };

    const onHistory = () => {
      dispatch({
        type: 'calculator/getResults'
      });
      dispatch({
        type: 'calculator/updateState',
        payload: {
          modalVisible: true
        }
      });
    };

    const deleteAllResult = () => {
      dispatch({
        type: 'calculator/deleteAllResult',
      })
    };


    const handleOk = () => {
      dispatch({
        type: 'calculator/updateState',
        payload: {
          modalVisible: false
        }
      })
    };

    const closeModal = () => {
      dispatch({
        type: 'calculator/updateState',
        payload: {
          modalVisible: false
        }
      })
    };


    const visibleColumns = [
      {
        title: 'Results',
        dataIndex: 'line',
        key: 'line'
      },
      {
        title: 'Operation',
        key: 'operation',
        render: (text, record) => <div>
          <Button type="danger" onClick={() => deleteResult(record.id)}><Icon type="delete"/></Button>
        </div>
      },
    ];

    return (
      <div className="container mt-2">
        <div className="row">
          <div className="col-md-4 offset-4">
            <div className="row mt-3">
              <input className="inText text-right" value={inputValue}/><br/>
              <input className="text-muted disVal text-right" disabled value={displayValue}/>
            </div>
            <div className="row">
              <button className="btn btn-primary clean" onClick={() => clearKey()}>C</button>
              <button className="btn btn-warning cleanAll" onClick={() => allClearKey()}>CA</button>
              <button className=" btn btn-info clean" onClick={handleMinus}>-</button>
              <button className="btn btn-outline-info clean" onClick={() => onHistory()}>HISTORY</button>
            </div>
            <div className="row">
              <button className="btn btn-default btnKey" onClick={() => onKey('7')}>7</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('8')}>8</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('9')}>9</button>
              <button className="btn btn-danger btnKey" onClick={() => onKeyOp('+')}>+</button>
            </div>
            <div className="row">
              <button className="btn btn-default btnKey" onClick={() => onKey('4')}>4</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('5')}>5</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('6')}>6</button>
              <button className="btn btn-danger btnKey" onClick={() => onKeyOp('-')}>-</button>
            </div>
            <div className="row">
              <button className="btn btn-default btnKey" onClick={() => onKey('1')}>1</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('2')}>2</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('3')}>3</button>
              <button className="btn btn-danger btnKey" onClick={() => onKeyOp('*')}>*</button>
            </div>
            <div className="row">
              <button className="btn btn-default btnKey" onClick={() => onKey('.')}>.</button>
              <button className="btn btn-default btnKey" onClick={() => onKey('0')}>0</button>
              <button className="btn btn-success btnKeyEq" onClick={equalsKey}>=</button>
              <button className="btn btn-danger btnKey" onClick={() => onKey('/')}>/</button>
            </div>
          </div>
        </div>
        <Modal
          title="Basic Modal"
          visible={modalVisible}
          onOk={handleOk}
          footer={null}
          onCancel={closeModal}
          >
          <div className="row">
            <div className="col-md-12">
              <Table dataSource={resultList} columns={visibleColumns} pagination={false}/>
              {
              resultList.length===0?"":
              <button onClick={() => deleteAllResult()} className="btn btn-danger">Delete All</button>}
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
