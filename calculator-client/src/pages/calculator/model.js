import {getResults,saveResult,deleteResult,deleteAllResult} from "./service";

export default {
  namespace: 'calculator',

  state: {
    inputValue:'',
    resultList:[],
    displayValue:'',
    res:'',
    modalVisible: false,
    signOp: false,
    equalkey:false

},

  subscriptions: {},

  effects: {
    * getResults({payload}, {call, put, select}) {
      const data = yield call(getResults,payload);
      console.log(data.data);
      yield put({
        type: 'updateState',
        payload: {
          resultList: data.data
        }
      })
    },

    * saveResult({payload}, {call, put, select}) {
      const data = yield call(saveResult,payload);
      yield put({
        type: 'getResults',
      });
    },

    * deleteResult({payload}, {call, put, select}) {
      const data = yield call(deleteResult, payload);
      console.log(data)
      yield put({
        type: 'getResults'
      })
    },

    * deleteAllResult({payload}, {call, put, select}) {
      const data = yield call(deleteAllResult);
      console.log(data)
      yield put({
        type: 'getResults'
      })
    },

  },

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }
}
