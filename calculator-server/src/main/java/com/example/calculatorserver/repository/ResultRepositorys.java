package com.example.calculatorserver.repository;

import com.example.calculatorserver.entity.Result;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ResultRepositorys extends JpaRepository<Result, UUID> {
}
