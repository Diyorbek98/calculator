package com.example.calculatorserver.controller;

import com.example.calculatorserver.entity.Result;
import com.example.calculatorserver.payload.ApiResponse;
import com.example.calculatorserver.payload.ReqResult;
import com.example.calculatorserver.repository.ResultRepositorys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/result")
public class ResultController {

    @Autowired
    ResultRepositorys resultRepository;

    @GetMapping
    public HttpEntity<?> getResults(){
        return ResponseEntity.ok(resultRepository.findAll());
    }

    @PostMapping
    public HttpEntity<?> addResult(@RequestBody ReqResult reqResult){
        try {
            Result result=new Result();
            result.setLine(reqResult.getLine());
            resultRepository.save(result);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteResult(@PathVariable UUID id){
        try {
            resultRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    @DeleteMapping
    public HttpEntity<?> deleteAllResult(){
        try {
            resultRepository.deleteAll();
            return ResponseEntity.ok(new ApiResponse("success", true));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }
}
